﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;          // <-- NuGet-Packet
using Newtonsoft.Json.Linq;     // <-- NuGet-Packet

namespace ConsoleAppJSONData
{

    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            

            List<Person> Personenliste = new List<Person>();

            Personenliste.Add(new Person() { Vorname = "Donald", Nachname = "Duck" });
            Personenliste.Add(new Person() { Vorname = "Daniel", Nachname = "Düsentrieb" });
            Personenliste.Add(new Person() { Vorname = "René", Nachname = "ó le" });

            String json = JsonConvert.SerializeObject(Personenliste);

            Console.WriteLine(json);

            List<Person> SPersonenListe = new List<Person>();
            SPersonenListe = JsonConvert.DeserializeObject<List<Person>>(json);

            foreach (Person p in SPersonenListe)
            {
                Console.WriteLine("Vorname: {0}, Nachname: {1} ",p.Vorname, p.Nachname);
            }


            Console.ReadLine();
        }
    }

}
