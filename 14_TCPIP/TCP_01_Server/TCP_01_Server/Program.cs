﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace TCP_01_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse("127.0.0.1");
                // use local m/c IP address, and 
                // use the same in the client

                /* Initializes the Listener */
                TcpListener myList = new TcpListener(ipAd, 8001);

                /* Start Listeneting at the specified port */
                myList.Start();

                Console.WriteLine("Der Server läuft nun auf Port 8001...");
                Console.WriteLine("Der lokalen Endpunkt ist  :" + myList.LocalEndpoint);
                Console.WriteLine("Warte auf eine Verbindung.....");

                while (true)
                {

                    Socket s = myList.AcceptSocket();
                    Console.WriteLine("Verbindung akzeptiert von: " + s.RemoteEndPoint);

                    byte[] b = new byte[100];
                    int k = s.Receive(b);
                    Console.WriteLine("Recieved...");
                    for (int i = 0; i < k; i++)
                        Console.Write(Convert.ToChar(b[i]));

                    ASCIIEncoding asen = new ASCIIEncoding();
                    s.Send(asen.GetBytes("Die Nachricht wurde emfpangen"));
                    Console.WriteLine("\nSende ACK");
                    /* clean up */
                    s.Close();
                    
                }
                myList.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }
    }
}
