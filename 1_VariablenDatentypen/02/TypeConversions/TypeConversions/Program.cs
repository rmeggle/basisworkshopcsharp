﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeConversions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Implicit Conversions"
            // Übersicht, was implizit zum konvertieren geht: 
            //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/implicit-numeric-conversions-table

            int EineZahl = 14;
            long EineSehrGroßeZahl = EineZahl;

            #endregion

            #region "Explicit Conversions"
            double x;
            int a;
            int b; 
            x = 12345.6;

            a = (int)x;             // 12345
            b = Convert.ToInt32(x); // 12346

            x = 270;
            byte c1 = (byte)x;           // Ergebnis: 14 ( da Überlauf)
            //byte c2 = Convert.ToByte(x); // Exception!!! ( da Überlauf)

            // Bei String's: 
            String s;
            s = "123";

            a = Int32.Parse(s);
            // Die Methode "TryParse" liefert true/false ob es erfolgreich war: 
            Int32.TryParse(s, out b);

            s = String.Empty;

            // a = Int32.Parse(s); // Exception!
            // a = Convert.ToInt32(s); // Exception!

            // Die Methode "TryParse" liefert true/false ob es erfolgreich war: 
            if (Int32.TryParse(s, out b))
            {
                // hat geklappt
            }
            else
            {
                // hat nicht geklappt
            };

            #endregion
        }
    }
}
