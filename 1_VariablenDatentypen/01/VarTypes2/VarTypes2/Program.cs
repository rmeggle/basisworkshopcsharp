﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VarTypes2
{
    class Program
    {

        public const double pi = 3.14159;

        static void Main(string[] args)
        {
            // Superglobale Variable
            String version = Global.Programmversion;
            Console.WriteLine("Die Version des programmes ist {0}", version);
            //Kreisberechnung:
            int r = 15;
            double A = 2 * pi * r;

            Console.WriteLine("Der Umfang des Kreises mit Radius {0} ist {1}", r, A);

            // Führt zu einem Fehler, eine Konstante kann nicht überschrieben werden
            // pi = 3.14;

            // Ausführen von einer "Prozedur":
            LokaleOderGlobaleVariable();
            Console.ReadLine();

        }

        /// <summary>
        /// Kritische Betrachtungen, warum man mit globalen Variablen vorsichtig sein sollte!
        /// </summary>
        static void LokaleOderGlobaleVariable()
        {
            double pi = 3.14; // Hatten die wir nicht schon als globale Klassenvariable?
            String Programmversion = "0.1"; // Hatten wir sie nicht auch schon als (super)globale Variable?

            Console.WriteLine("Die Version des programmes ist {0}", Programmversion);

            //Kreisberechnung:
            int r = 15;
            double A = 2 * pi * r;

            Console.WriteLine("Der Umfang des Kreises mit Radius {0} ist {1}", r, A);

            

        }

    }
}
