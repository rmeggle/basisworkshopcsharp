﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VarTypes1
{
    class Program
    {

        enum Days { Sat, Sun, Mon, Tue, Wed, Thu, Fri };
        enum Tage { Sonntag = 0, Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag };

        enum MachineState
        {
            PowerOff = 0,
            Running = 5,
            Sleeping = 10,
            Hibernating = Sleeping + 5
        }

        static void Main(string[] args)
        {
            // Wichtig bei C# => So ziemlich alles ist ein Objekt!
            // Übersicht hier: https://msdn.microsoft.com/de-de/library/cs7y5x0x(v=vs.90).aspx     

            // Beispiele (...zum Diskutieren?): 
            int Antwort = 42;           // C# Datentyp, alias zu int32
            Int16 antwort16 = 42;       // .NET Datentyp
            Int32 antwort32 = 42;       // .NET Datentyp, alias zu int
            Int64 antwort64 = 42;       // .NET Datentyp

            string Frage = "Und die Frage war?";       // C# Datentyp
            String frage = "Und die Frage war?";       // .NET Datentyp


            // => Jede Variable muss nach/bei der Initialisierung einen Wert oder eine Instanz erhalten
            // => Es wird zwischen Groß- und Kleinschreibnung unterschieden. 
            // => Es wird zwischen Datentypen und Objekten unterschieden. 
            //    So ist beispielsweise "string" ein Datentyp, "String" ein CLR-Objekt. Hier eine Übersicht: 

            // object:  System.Object
            // string:  System.String
            // bool:    System.Boolean
            // byte:    System.Byte
            // sbyte:   System.SByte
            // short:   System.Int16
            // ushort:  System.UInt16
            // int:     System.Int32
            // uint:    System.UInt32
            // long:    System.Int64
            // ulong:   System.UInt64
            // float:   System.Single
            // double:  System.Double
            // decimal: System.Decimal
            // char:    System.Char

            // Wann aber was nehmen? Bleiben wir bei den Strings...

            int eineZahl = 4711;
            String EineAntwort = String.Format("Eine Zahl {0} ist definiert", eineZahl);

            // Aufzählungstypen "enumerationen":
            int US_Sonntagx = (int)Days.Sun;
            int US_Freitag  = (int)Days.Fri;
            int DE_Sonntag = (int)Tage.Sonntag;
            int DE_Freitag = (int)Tage.Freitag;

            Console.WriteLine("Sun = {0}", US_Sonntagx);
            Console.WriteLine("Fri = {0}", US_Freitag);

            Console.WriteLine("Sonntag = {0}", DE_Sonntag);
            Console.WriteLine("Freitag = {0}", DE_Freitag);

            Console.ReadLine();
        }
    }
}
