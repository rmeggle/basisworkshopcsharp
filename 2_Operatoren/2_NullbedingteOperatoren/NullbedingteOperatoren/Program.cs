﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullbedingteOperatoren
{
    class Program
    {
        static void Main(string[] args)
        {
            // Jeder C#-Entwickler weiß, wie man mit Datentypen int, double, boolean... arbeitet. 
            // Jedoch haben sie eine Besonderheit: Sie können nicht NULL als Wert haben. 

            // Folgende beide Befehle liefern einen Fehler: 
            //bool myBool = null;
            //DateTime myDate = null;

            // C# hat hierfür den sog. "nullable"
            // Beispiel mit traditioneller lösung

            // Die Operatoren ? und ?.
            String s = null;
            int? sl = s?.Length;

            // Arrays: 
            String[] MeinAarray = null;                         // Dieses Feld hat den Inhalt "NULL"
            
            int ArrayIndex = -1;                                // ArrayIndex auf -1 setzten
            if (MeinAarray != null && MeinAarray[1] != null)    // Prüfen ob das Array existiert, wenn ja dann: 
                ArrayIndex = MeinAarray[1].Length;              // Den Arrayindex auf das letzte Element setzten
            Console.WriteLine("Länge: " + ArrayIndex);          // Ausgabe der Länge des Arrays, hier "-1"

            // Der Operator ?[
            String Elementinhalt = MeinAarray?[1];              // Versucht auf Elem. Index 1 zuzugreifen (aber es existiert nicht!)

            int? lenMeinArray = MeinAarray?[1]?.Length;                // Versucht auf die Länge von einem NULL-Array zuzugreifen
            Console.WriteLine("Länge: " + lenMeinArray);               // Ausgabe: -nix- da null

            // Kombination mit dem Null-Sammeloperator
            int len1c = MeinAarray?[1]?.Length ?? -1;           // Diskussion? (Sonderform tenärer operator -> Setze len1c auf Indexelement ansonsten -1 )

            DateTime myDate = DateTime.Now;                     // Aktuelles Datum
            DateTime? myNullableDate = null;                    // null



            // Prüfen auf die Existenz auf null: 
            if (lenMeinArray.HasValue)
            {
                // foo
            }

            if (lenMeinArray == null)
            {
                // foo
            }

            Console.ReadLine();


        }
    }
}
