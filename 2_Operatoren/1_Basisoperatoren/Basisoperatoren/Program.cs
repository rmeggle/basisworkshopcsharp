﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Basisoperatoren
{
    class Program
    {
        static void Main(string[] args)
        {
            ArithmetischeOperatoren();
            RelationaleOperatoren();
            LogischeOperatoren();
            BitweiseOperatoren();
            ZeichenkettenOperatoren();
        }

        static void ArithmetischeOperatoren()
        {
            int x = 40;
            int y = 2;

            #region "Grundlegende Operatoren"
            //////////////////////////////////////////
            // Addition
            int summe = x + y;

            //////////////////////////////////////////
            // Subtraktion
            int differenz = x - y;

            //////////////////////////////////////////
            // Multiplikation

            int produkt = x * y;

            //////////////////////////////////////////
            // Division

            // Vorsicht... die  Variable ist nun ein int und kein double!!
            // Wie verhält es sich generell, wenn int durch int dividiert werden?
            int quotient = x / y;

            //////////////////////////////////////////
            // Modulo
            int restwert= x % y;
            #endregion

            #region "Auflösungsregeln"
            // Klammer vor Punkt vor Strich, aber ...
            int wasIst1 = 6 / 2 * 3;
            int wasIst2 = 6 / 2 * (1 + 2);

            #endregion

            #region "allgem. Kurzschreibweisen"

            x += 1;// entspricht x = x + 1
            x -= 1;// entspricht x = x - 1 
            x *= 1;// entspricht x = x * 1
            x /= 1;// entspricht x = x / 1



            #endregion

            #region "Kurzschreibweise: Post- und Preinkrement"

            // Häufig fehlinterpretiert: Diese beiden Zuweisungen eines 
            // post/preinkrementes sind identisch: In beiden Fällen ist der
            // Wert in y 2. 
            x = 1;  y = x++;
            x = 1;  y = ++x;

            // Anders verhält es sich bei unmittelbarer Verwendung als z.B. 
            // Parameter - hier als Parameter für die Methode WriteLine
            x = 1;
            Console.WriteLine(x++); // Ausgabe "1"
            x = 1;
            Console.WriteLine(++x); // Ausgabe "2"

            // oder bei dem Verwenden als Index in einem Array: 
            string[] programmiersprache = new string[] { "C", "C++", "C#" };
            int index = 0;
            Console.WriteLine(programmiersprache[index]);
            index = 1; Console.WriteLine(programmiersprache[index++]); // Ausgabe: C++
            index = 1; Console.WriteLine(programmiersprache[++index]); // Ausgabe: C#

            #endregion
        }

        static void RelationaleOperatoren()
        {
            int x = 47;
            int y = 11;
            bool result;

            // gleich
            result = x == y;

            // ungleich
            result = x != y;

            // größer
            result = x > y;

            // kleiner
            result = x < y;

            // größer gleich
            result = x >= y;

            // kleiner gleich
            result = x <= y;

            int CompareResult;
            // Hier, in C# spezifischem Konzept "alles ist ein Objekt": 
            // LessThan=-1, Equal=0, GreaterThan=1
            CompareResult = x.CompareTo(y);

        }

        static void LogischeOperatoren()
        {
            bool x = true;
            bool y = false;
            bool result;

            // UND:
            result = x && y;
            // ODER: 
            result = x || y;
            // XOR ( exklusives ODER ) - oft verwechselt mit exponent! Exponent ist in C#: Math.Pow(): 
            result = x ^ y; // XOR
            double exp = Math.Pow(2,0); // entspricht 2 hoch 0 = 1
            // NICHT
            result = !x;
        }

        static void BitweiseOperatoren()
        {
            int x = 1; // Binär: 000001
            int y = 3; // Binär: 000010
            int result;

            // und ( Ergebnis 1 wenn beide bit 1 sind)
            result = x & y;
            // oder ( Ergebnis 1 wenn (mind.) eines der beiden bit 1 ist)
            result = x | y;
            // xor ( Ergebnis 1 wenn genau eines der beiden bit 1 ist)
            result = x ^ y;
            // nicht  (Ergebnis 1 wenn das bit 0 und umgekehrt ist)
            result = ~y;
            // verschieben nach links
            result = x << 1;
            // verschieben nach rechts 
            result = x >> 1;


        }

        static void ZeichenkettenOperatoren()
        {
            String foo = "Hallo";
            String bar = "Welt";
            bool Bresult;
            int Iresult;

            // Ist String foo identisch mit bar?
            Bresult = (foo == bar);
            // Ist String foo nicht identisch mit bar?
            Bresult = (foo != bar);
            // Stringzusammensetzungen: 
            String foobar = foo + bar;

            //
            // Alternativen bei Stringoperatoren
            //
            int CompareResult;
            // Hier, in C# spezifischem Konzept "alles ist ein Objekt": 
            // LessThan=-1, Equal=0, GreaterThan=1
            CompareResult = foo.CompareTo(bar);

            // ReferenceEquals liefert true, wenn beide Strings im Speicher unter der gleichen Adresse zu finden sind
            Bresult = ReferenceEquals(foo, bar);

            // Spezifische Methoden der String.Compare Methode.
            // String.Compare: Vergleicht zwei angegebene String-Objekte, wobei ihre Groß- und Kleinschreibung 
            // entweder ignoriert oder berücksichtigt wird, und gibt eine Ganzzahl zurück, welche die relative Position in der Sortierreihenfolge angibt.
            // LessThan=-1, Equal=0, GreaterThan=1
            Iresult = String.Compare(foo, bar);
            Iresult = String.Compare(foo, bar, StringComparison.CurrentCulture);
            Iresult = String.Compare(foo, bar, StringComparison.CurrentCultureIgnoreCase);
            Iresult = String.Compare(foo, bar, StringComparison.Ordinal);
            Iresult = String.Compare(foo, bar, StringComparison.OrdinalIgnoreCase);

            // case-sensitiver Vergleich mit der String.CompareOrdinal Methode
            Iresult = String.CompareOrdinal(foo, bar);

            // String.Equals: Bestimmt, ob diese Stringinstanz und ein anderes angegebenes 
            // Stringobjekt denselben Wert haben.
            Bresult = String.Equals(foo, bar, StringComparison.CurrentCulture);
            Bresult = String.Equals(foo, bar, StringComparison.CurrentCultureIgnoreCase);
            Bresult = String.Equals(foo, bar, StringComparison.Ordinal);
            Bresult = String.Equals(foo, bar, StringComparison.OrdinalIgnoreCase);

            // Zusammenfassende Gegenüberstellung:
            foo = "Hallo Welt";
            bar = "Hallo Welt";
            String[] Foo = { foo.ToLower(), foo, foo.ToUpper() };
            foreach (String f in Foo)
            {
                Bresult = f.Equals(bar);                                                // beachtet Groß-Kleinscheibung
                Bresult = f.Equals(bar, StringComparison.CurrentCulture);               // beachtet Groß-Kleinscheibung
                Bresult = f.Equals(bar, StringComparison.CurrentCultureIgnoreCase);     // Ignoriert Groß-Kleinscheibung
                Bresult = (foo == bar);                                                 // Ignoriert Groß-Kleinscheibung
                Bresult = f.Equals(bar);                                                // Ignoriert Groß-Kleinscheibung
                Iresult = String.Compare(foo, bar);                                     // Ignoriert Groß-Kleinscheibung
                Iresult = String.Compare(foo, bar, StringComparison.CurrentCulture);    // Ignoriert Groß-Kleinscheibung
            }

            foo = "Hallo";
            bar = "Welt";

            // Stringzusammensetzungen: 
            String foobar = foo + bar;

            // Objektspezifisch: 
            foobar = String.Concat(foo, bar);

            // Mit Hilfe des String-Builder-Objektes. Vorteil: gefühlt 100x schneller!
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(foo);
            sb.Append(" ");
            sb.Append(bar);
            sb.Clear();
            sb.Append(foo).Append(" ").Append(bar);
            foobar = sb.ToString();
        }

    }
}
