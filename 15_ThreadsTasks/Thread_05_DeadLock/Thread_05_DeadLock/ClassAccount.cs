﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_05_DeadLock
{
    class ClassAccount
    {
        private double balance;
        private int id;

        public int ID { get { return id; } }

        public ClassAccount(int id, int balance)
        {
            this.id = id;
            this.balance = balance;
        }

        public void Abheben(double betrag)
        {
            Console.WriteLine("Thread " + Thread.CurrentThread.Name + " hat " + betrag + " abgehoben");
            balance -= betrag;
        }

        public void Einzahlen(double betrag)
        {
            Console.WriteLine("Thread " + Thread.CurrentThread.Name + " hat " + betrag + " eingezahlt");
            balance += betrag;
        }
    }

    internal class KontoManager
    {
        private ClassAccount VonKonto;
        private ClassAccount NachKonto;
        private int Betrag;

        public KontoManager(ClassAccount von, ClassAccount nach, int betrag)
        {
            this.VonKonto = von;
            this.NachKonto = nach;
            this.Betrag = betrag;
        }

        public void Überweisung()
        {
            Console.WriteLine("Thread " + Thread.CurrentThread.Name + " ist gestartet");

            // Start des "nested lock's"... Beginn allem Übels... 
            // "etwas" bessere Variante: Verwenden von AutoResetEvent und saubere Aufrufe
            lock (VonKonto)
            {
                Console.WriteLine("Thread " + Thread.CurrentThread.Name + " sperrt ressource >VonKonto<");
                Thread.Sleep(1000); // kurze Pause machen - damit der andere Thread nicht aufholt
                lock (NachKonto)
                {
                    Console.WriteLine("Thread " + Thread.CurrentThread.Name + " sperrt ressource >NachKonto<");
                    VonKonto.Abheben(Betrag);
                    NachKonto.Einzahlen(Betrag);
                }
            }
        }
        /*
         * Grundregeln für das Vermeiden von DeadLocks
         * 
         * Keine öffentliche Methoden (public member) sperren. Ansonsten wird es sehr schwer
         * festzustellen, wer noch eine Methode gesperrt hat. 
         * 
         * Konkurrierende Prozesse vermeiden - verwenden von "WaitHandle"s -> AutoResetEvents
         * 
         * Ressourcen der Zugriffe nach ordnen, solange es noch die Möglichkeit dazu gibt. 
         * 
         * Vorsicht bei "overwriteable" Methoden und Monitor, lock's etc. Es wird sehr
         * schwer, hier noch die Übersicht zu wahren. 
         * 
         */

    }
}
