﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_1_Basics
{
    class ClassCalculations
    {
        public static void M1()
        {
            Console.WriteLine("\nM1 beginnt mit der Arbeit:");
            for (int i = 1; i <= 30; i++)
            {
                Console.Write("(M1) ");
                Thread.Sleep(500);
            }
            Console.WriteLine("\nM1 beendet");
        }

        public static void M2()
        {
            Console.WriteLine("\nM2 beginnt mit der Arbeit");

            for (int i = 1; i <= 30; i++)
            {
                Console.Write("(M2) ");
                Thread.Sleep(500);
            }
            Console.WriteLine("\nM2 beendet");
        }
    }
}
