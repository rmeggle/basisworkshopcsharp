﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_1_Basics
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(ClassCalculations.M1));
            Thread t2 = new Thread(new ThreadStart(ClassCalculations.M2));

            t1.Start();
            t2.Start();

            Console.ReadLine();
        }
    }
}
