﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_06_Abort
{
    class Program
    {

        static void TestMethod()
        {
            try
            {
                while (true)
                {
                    Console.Write("renne...");
                    Thread.Sleep(50);
                }
            }
            catch (ThreadAbortException abortException)
            {
                String Exception = (string)abortException.ExceptionState;
                Console.WriteLine("Bin abgewürgt worden: " + Exception);
            }
        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(TestMethod));
            t1.Name = "Thread 1";
            t1.Start();

            Thread.Sleep(1000);

            Console.WriteLine("Beenden des Threads " + t1.Name);
            t1.Abort("Grund: Du dauerst mir zu lange...");

            // Wait for the thread to terminate.
            t1.Join();
            Console.WriteLine("New thread terminated - Main exiting.");

            Console.ReadLine();
        }
    }
}
