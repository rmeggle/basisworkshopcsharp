﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Will ein Thread in den Wartezustand wechslen, bis ein anderer Thread abgeschlossen ist, kann 
/// er dies mit dem Aufruf der Thread-Methode "Join()" realisieren. 
/// Im Aufrufenden Teil ist kein Unterschied - in der ClassCalculations wurde der Join in M2 eingefügt. 
/// </summary>
namespace Thread_2_Joins
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassCalculations.t1 = new Thread(new ThreadStart(ClassCalculations.M1));
            ClassCalculations.t2 = new Thread(new ThreadStart(ClassCalculations.M2));

            ClassCalculations.t1.Start();
            ClassCalculations.t2.Start();

            Console.ReadLine();
        }
    }
}
