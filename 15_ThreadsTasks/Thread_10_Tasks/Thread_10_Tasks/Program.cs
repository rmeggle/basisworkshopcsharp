﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_10_Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            // Action delegate und benanntem Methodenaufruf
            // (action delegate akzeptiert keine Parameter...) 
            Task task1 = new Task(new Action(printStandardMessage));
            // anonymen delegate (Parameterangabe innerhalb von { ... } )
            Task task2 = new Task(delegate { printStandardMessage(); });
            // Lambda-Expression und benanntem Methodenaufruf
            Task task3 = new Task(() => printStandardMessage());
            //  Lambda-Expression und anonymen Methodenaufruf
            Task task4 = new Task(() => { printStandardMessage(); });

            task1.Start();
            task2.Start();
            task3.Start();
            task4.Start();

            // Warten auf Tasks:
            Task.WaitAll(task1, task2, task3, task4);

            // Tasks mit Parameter zum delegate
            Task task5 = new Task(() => { print("Hallo Welt"); });
            task5.Start();

            // Ausführen eines anderen (async) Task, sobald 
            // der erste fertig ist:
            Task.Factory.StartNew(() => { print("erster Task"); })
                        .ContinueWith((t1) => { print("zweiter Task"); })
                        .ContinueWith((t2) => { print("dritter Task"); })
                        .Wait(3000 /*ms Timeout */);

            Console.ReadLine();
        }


        private static void print(string Param = "task")
        {
            Console.WriteLine(Param);
        }
        private static void printStandardMessage()
        {
            Console.WriteLine("task");
            System.Threading.Thread.Sleep(1000);
        }

    }
}
