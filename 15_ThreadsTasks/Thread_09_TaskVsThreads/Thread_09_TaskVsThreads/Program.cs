﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_09_Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = new Stopwatch();
            const int maxWaitHandleWaitAllAllowed = 64;
            var mres = new ManualResetEventSlim[maxWaitHandleWaitAllAllowed];

            for (var i = 0; i < mres.Length; i++)
            {
                mres[i] = new ManualResetEventSlim(false);
            }
            watch.Start();
            //start a new classic Thread and signal the ManualResetEvent when its done
            //so that we can snapshot time taken, and 
            for (var i = 0; i < mres.Length; i++)
            {
                var idx = i;
                var t = new Thread(state =>
                {
                    for (var j = 0; j < 10; j++)
                    {
                        Console.WriteLine(string.Format("Thread : {0}, outputing {1}", state.ToString(), j.ToString()));
                    }
                    mres[idx].Set();
                });
                t.Start(string.Format("Thread{0}", i));
            }

            WaitHandle.WaitAll((from x in mres select x.WaitHandle).ToArray());

            var threadTime = watch.ElapsedMilliseconds;
            watch.Reset();

            foreach (ManualResetEventSlim t in mres)
            {
                t.Reset();
            }
            watch.Start();

            for (var i = 0; i < mres.Length; i++)
            {
                var idx = i;
                Task.Factory.StartNew(state =>
                {
                    for (var j = 0; j < 10; j++)
                    {
                        Console.WriteLine(string.Format("Task : {0}, outputing {1}", state.ToString(), j.ToString()));
                    }
                    mres[idx].Set();
                }, string.Format("Task{0}", i));

            }

            WaitHandle.WaitAll((from x in mres select x.WaitHandle).ToArray());
            var taskTime = watch.ElapsedMilliseconds;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Traditional Thread Approach   : {0}ms", threadTime);
            Console.WriteLine("Task Parallel Library Approach: {0}ms", taskTime);

            Console.ForegroundColor = ConsoleColor.White;

            foreach (var t in mres)
            {
                t.Reset();
            }

            Console.ReadLine();
        }
    }
}
