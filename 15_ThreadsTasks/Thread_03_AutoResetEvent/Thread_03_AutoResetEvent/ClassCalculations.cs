﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Über ein Signalisierungsobjekt der Klasse "AutoResetEvent" kann sich ein Thread in 
/// einen vorübergehenden Ruhezustand versetzten => bis ein anderer Thread ihn wieder aktiviert. 
/// 
/// WaitOne()   -> Erteilt Weckauftrag an einen anderen Thread
/// Set()       -> Aktivieren eines wartenden Threads
/// Reset()     -> Signal bleibt erhalten, bis es mit Reset() storniert wird
/// 
/// </summary>
namespace Thread_03_AutoResetEvent
{
    class ClassCalculations
    {

        public static Thread t1, t2;
        static AutoResetEvent myAutoResetEvent = new AutoResetEvent(false); // Signal ist initial aus.


        public static void M1()
        {
            // M1 geht in den Wartezustand und macht nur weiter, 
            // wenn er wieder geweckt wird
            myAutoResetEvent.WaitOne(); // Blokieren des aktuellen Threads bis das WaitHandle wieder das Startsignal erhält
            Console.WriteLine("\nM1 beginnt mit der Arbeit:");
            for (int i = 1; i <= 3; i++)
            {
                Console.Write("(M1) ");
                Thread.Sleep(500);
            }
            Console.WriteLine("\nM1 beendet");
        }

        public static void M2()
        {
            Console.WriteLine("\nM2 beginnt mit der Arbeit");

            for (int i = 1; i <= 3; i++)
            {
                Console.Write("(M2) ");
                Thread.Sleep(500);
            }
            Console.WriteLine("\nM2 beendet");
            // M2 weckt nun wieder M1: 
            myAutoResetEvent.Set();

        }

        public static void M()
        {
            int t = 1;
            if (Thread.CurrentThread == t2)
            {
                t = 2;
                Console.WriteLine("\n" + Thread.CurrentThread.Name + " geht als erster in den Wartzuestand.");
                myAutoResetEvent.WaitOne(); // Blokieren des aktuellen Threads bis das WaitHandle wieder das Startsignal erhält
            }

            for (int j = 1; j <= 2; j++)
            {
                Console.Write("\n" + Thread.CurrentThread.Name + " ist am Zug: ");
                for (int i = 1; i <= 5; i++)
                {
                    Console.Write(t + " ");
                    Thread.Sleep(200);
                }
                Console.WriteLine("\n" + Thread.CurrentThread.Name + " Setzt das Signal und dann sich selbst zur Ruhe.");
                myAutoResetEvent.Set();

                Thread.Sleep(10); // Nötig, damit der andere Thread das Signal verbraucht.

                Console.WriteLine("\n" + Thread.CurrentThread.Name + " wartet nun...");
                myAutoResetEvent.WaitOne();
            }
            Console.WriteLine("\n" + Thread.CurrentThread.Name + " endet.");
            myAutoResetEvent.Set(); // Nötig, damit der wartende Kollege zum letzten Auftritt geweckt wird.
        }



    }
}
