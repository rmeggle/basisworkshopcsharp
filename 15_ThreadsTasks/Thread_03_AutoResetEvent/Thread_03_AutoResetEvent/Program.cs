﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_03_AutoResetEvent
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Aufruf zweier unabhängiger Methoden die auf sich gegenseitig warten"
            Thread t1 = new Thread(new ThreadStart(ClassCalculations.M1));
            Thread t2 = new Thread(new ThreadStart(ClassCalculations.M2));

            t1.Name = "M1";
            t2.Name = "M2";

            t1.Start();
            t2.Start();
            #endregion

            #region "Eine statische methode, zweimal instanziert und auf sich wartet"

            //ClassCalculations.t1 = new Thread(ClassCalculations.M);
            //ClassCalculations.t2 = new Thread(ClassCalculations.M);

            //// Vergeben der Threads einen eindeutigen Namen 
            //// zur besseren Übersicht
            //ClassCalculations.t1.Name = "t1";
            //ClassCalculations.t2.Name = "t2";

            //ClassCalculations.t1.Start();
            //ClassCalculations.t2.Start();

            #endregion

            Console.ReadLine();


        }
    }
}
