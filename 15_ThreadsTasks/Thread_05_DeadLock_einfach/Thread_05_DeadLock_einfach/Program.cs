﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Thread_05_DeadLock_einfach
{
    class Program
    {
        static object lock1 = new object();
        static object lock2 = new object();

        static void M1()
        {
            lock (lock1)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " in M1");
                // Kurz schlafen, so dass beide Threads ihren "eigenen" synchronisierten Block un-
                // gestört betreten können.Anschließend versuchen beide, in den jeweiligen fremden Block zu gelangen, 
                // --> und das Programm hängt fest
                Thread.Sleep(100); 
                Console.WriteLine(Thread.CurrentThread.Name + " möchte M2 aufrufen");
                M2();
            }
        }

        static void M2()
        {
            lock (lock2)
            {
                Console.WriteLine(Thread.CurrentThread.Name + " in M2");
                // Kurz schlafen, so dass beide Threads ihren "eigenen" synchronisierten Block un-
                // gestört betreten können.Anschließend versuchen beide, in den jeweiligen fremden Block zu gelangen, 
                // --> und das Programm hängt fest
                Thread.Sleep(100);
                Console.WriteLine(Thread.CurrentThread.Name + " möchte M1 aufrufen");
                M1();
            }
        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(M1));
            Thread t2 = new Thread(new ThreadStart(M2));
            t1.Name = "T1"; t1.Start();
            t2.Name = "T2"; t2.Start();
        }
    }
}
