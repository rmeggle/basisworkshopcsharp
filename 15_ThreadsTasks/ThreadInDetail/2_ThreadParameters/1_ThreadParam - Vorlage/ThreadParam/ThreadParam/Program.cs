﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadParam
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(arbeite);
            t1.Start();
        }

        static void arbeite()
        {
            int anzahl = 10; // diese Anzahl soll dem Arbeiter übergeben werden
            for (int i = 0; i < anzahl; i++)
            {
                Console.WriteLine("Arbeite...");
            }
        }
    }
}
