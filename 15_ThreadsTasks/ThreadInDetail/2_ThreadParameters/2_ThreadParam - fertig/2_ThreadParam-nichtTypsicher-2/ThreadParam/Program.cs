﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadParam
{
    class Program
    {
        static void Main(string[] args)
        {

            // 1. Schritt: Parameter wird vorbereitet
            object parameter = 2;

            // Hinweis: ParameterizedThreadStart -> Muss nicht unbedingt angegeben werden

            Thread t1 = new Thread(arbeite);
            t1.Start(parameter); // Start hat überladungen - eine davon ist für Parameter

            Console.ReadLine();
        }

        // der Delegate schreibt uns ein Objekt voraus! Daher: 
        // static void arbeite(int AnzahlObjekt) // <------------ so geht das nicht! 
        static void arbeite(object AnzahlObjekt)
        {
            int anzahl = 0; 

            // TryParse liefert true/false wenn geglückt - out nicht vergessen, da referenzübergabe)
            if (int.TryParse(AnzahlObjekt.ToString(), out anzahl))
            {
                for (int i = 0; i < anzahl; i++)
                {
                    Console.WriteLine("Arbeite...");
                }
            }
        }
    }
}
