﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTaskContinue
{
    class Program
    {
        static void Main(string[] args)
        {
            // M1, M2, M3 und M4 sollen unmittelbar nacheinander und nicht 
            // Parallel ausgeführt werden. 
            // Die Rückgabe von M4 soll ausgewertet werden
            Task task1 = Task.Run((() => { M1(); }));

            Task task2 = Task.Run(() => { M2(); });

            Task task3 = Task.Run(() => { M3("Param1", "Param2", 4711); });

            Task<int> task4 = Task<int>.Run(() => {
                int ret = Addiere(1, 1);
                return ret;
            });

            Task.WaitAll(task1, task2, task2, task4);

            Console.WriteLine("Das Ergebnis der Addition war: " + task4.Result);
            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        static void M1()
        {
            String Ausgabe = " M1 arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe);
        }

        static void M2()
        {
            String Ausgabe = " M2 arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe);
        }

        static void M3(String p1, String p2, Int32 p3)
        {
            String Ausgabe = " ...und nun arbeite ich...";
            Console.WriteLine(Task.CurrentId + Ausgabe + p1 + p2 + Convert.ToString(p3));
        }

        static int Addiere(int Zahl1, int Zahl2)
        { 
              return Zahl1 + Zahl2;
        }
    }
}
