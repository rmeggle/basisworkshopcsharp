﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTaskContinue
{
    class Program
    {
        static void Main(string[] args)
        {
            // M1, M2, M3 und M4 sollen unmittelbar nacheinander und nicht 
            // Parallel ausgeführt werden. 
            // Die Rückgabe von M4 soll ausgewertet werden
            var ErsterTask = Task.Run((() => { M1(); }));

            var ZweiterTask = ErsterTask.ContinueWith((t1) => { M2(); });

            var DritterTask = ZweiterTask.ContinueWith((t3) => { M3("Param1", "Param2", 4711); });

            var VierterTask = DritterTask.ContinueWith((t4) => {
                int ret = Addiere(1, 1);
                return ret;
            });

            VierterTask.Wait();

            Console.WriteLine("Das Ergebnis der Addition war: " + VierterTask.Result);
            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        static void M1()
        {
            String Ausgabe = " M1 arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe);
        }

        static void M2()
        {
            String Ausgabe = " M2 arbeitet...";
            Console.WriteLine(Task.CurrentId + Ausgabe);
        }

        static void M3(String p1, String p2, Int32 p3)
        {
            String Ausgabe = " ...und nun arbeite ich...";
            Console.WriteLine(Task.CurrentId + Ausgabe + p1 + p2 + Convert.ToString(p3));
        }

        static int Addiere(int Zahl1, int Zahl2)
        { 
              return Zahl1 + Zahl2;
        }
    }
}
