﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDataParallel
{
    class Program
    {
        static void Main(string[] args)
        {
            ParallelFor();
            Console.ReadLine();
        }

        static void ParallelFor()
        {
            Stopwatch timePerParse;

            #region "for Schleife"

            // Stoppuhr starten
            timePerParse = Stopwatch.StartNew();

            int N = 10000000;
            double sum = 0.0;
            double step = 1.0 / N;

            for (var i = 0; i < N; i++)
            {
                double x = (i + 0.5) * step;
                sum += 4.0 / (1.0 + x * x);
            }

            timePerParse.Stop();

            Console.WriteLine(sum * step);
            Console.WriteLine("Verbrauchte ticks: "+ timePerParse.ElapsedTicks);
            #endregion
            // ----

            #region "ParallelFor"

            // Stoppuhr starten
            timePerParse = Stopwatch.StartNew();

            object LOCK = new object();
            sum = 0.0;
            step = 1.0 / N;

            Parallel.For(0, N, i =>
                    {
                        double x = (i + 0.5) * step;
                        double y = 4.0 / (1.0 + x * x);
                        lock (LOCK)
                        {
                            sum += y;
                        }
                    });

            timePerParse.Stop();

            Console.WriteLine(sum * step);
            Console.WriteLine("Verbrauchte ticks: " + timePerParse.ElapsedTicks);

            #endregion


            #region "ParallelFor (optimiert)"

            // Stoppuhr starten
            timePerParse = Stopwatch.StartNew();

            sum = 0.0;
            step = 1.0 / N;

            Parallel.For(0, N, () => 0.0, (i, state, local) =>
                {
                    double x = (i + 0.5) * step;
                    return local + 4.0 / (1.0 + x * x);
                }, local =>
                {
                    lock (LOCK)
                    {
                        sum += local;
                    }
                });

            timePerParse.Stop();

            Console.WriteLine(sum * step);
            Console.WriteLine("Verbrauchte ticks: " + timePerParse.ElapsedTicks);

            #endregion
        }
    }
}
