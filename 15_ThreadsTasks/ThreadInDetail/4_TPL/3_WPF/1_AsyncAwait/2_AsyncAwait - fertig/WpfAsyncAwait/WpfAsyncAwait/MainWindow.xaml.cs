﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAsyncAwait
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Label.Content = "...arbeite";

            Task<int> t1 = new Task<int>(ZähleIrgendwas);
            t1.Start();

            int Ergebnis = await t1;

            TextBox.Text = Convert.ToString(Ergebnis);

            Label.Content = "fertig!";
        }

        private int ZähleIrgendwas()
        {
            int i;

            for (i = 0; i < 10; i++)
            {
                Thread.Sleep(500);
            }
            return i;
        }
    }
}
