﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppLock
{
    class Program
    {
        public static int anzahl= 0;

        public static void arbeite()
        {
            for (int i = 0; i < 100000; i++)
            {
                Interlocked.Increment(ref anzahl);
            }
        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(arbeite);
            Thread t2 = new Thread(arbeite);
            Thread t3 = new Thread(arbeite);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.WriteLine("Gesamtanzahl : " + anzahl);

            Console.ReadLine();
        }
    }
}
