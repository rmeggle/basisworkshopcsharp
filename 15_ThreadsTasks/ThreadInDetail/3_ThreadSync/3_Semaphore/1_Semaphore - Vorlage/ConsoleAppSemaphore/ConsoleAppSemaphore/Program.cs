﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppSemaphore
{
    class Program
    {

        public static int anzahl = 0;

        static void Main(string[] args)
        {
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        public static void arbeite()
        {
            for (int i = 0; i < 100000; i++)
            {
                anzahl++;
            }
        }
    }
}
