﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppSemaphore
{
    class Program
    {
        /// <summary>
        /// Es gibt SemaphoreSlim und Semaphore. SemaphoreSlim wurde in .NET 4 eingeführt. 
        /// Der Unterschied ist der weitaus geringere Ressourcenverbrauch im gegensatz 
        /// zu ursprünglichen Semaphore.
        /// 
        /// Eine Semaphore hat im Gegensatz zu lock/Monitor keinen Besitzer. Somit
        /// kann der Semaphore mitgeteilt werden, wie viele gleichzeitige Zugriffe wir
        /// auf einen Codeabschlitt zulassen wollen. 
        /// </summary>
        static SemaphoreSlim semaphore = new SemaphoreSlim(1); // nur ein Thread darf hinein!

        public static int anzahl = 0;

        static void Main(string[] args)
        {
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        public static void arbeite()
        {
            semaphore.Wait();
            for (int i = 0; i < 100000; i++)
            {
                anzahl++;
            }
            semaphore.Release();
        }
    }
}
