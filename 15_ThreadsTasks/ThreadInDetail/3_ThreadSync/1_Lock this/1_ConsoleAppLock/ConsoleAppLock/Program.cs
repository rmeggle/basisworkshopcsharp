﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppLock
{
    class ClassArbeiter
    {
        public static int anzahl = 0;

        public void arbeite()
        {
            lock (this)
            {
                for (int i = 0; i < 100000; i++)
                {
                    anzahl++;
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
        }

        public static void arbeite()
        {
            ClassArbeiter arbeiter = new ClassArbeiter();
            Thread t1 = new Thread(arbeiter.arbeite);
            t1.Start();
            
            // ... und im weiteren Codeabschnitt / andere Classe zur gleichen Zeit:
            
            lock (arbeiter) // was ist nun der Unterschied zwischen "this" und "arbeiter"?
            {
                arbeiter.arbeite();
            }
            Console.ReadLine();
        }
    }
}
