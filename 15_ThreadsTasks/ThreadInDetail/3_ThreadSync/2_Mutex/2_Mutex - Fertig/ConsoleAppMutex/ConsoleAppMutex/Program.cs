﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppMutex
{
    class Program
    {
        private static Mutex mutex = new Mutex(false,"Programm ID");
        
        public static int anzahl = 0;

        static void Main(string[] args)
        {
            new Thread(arbeite).Start();
            new Thread(arbeite).Start();

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }

        public static void arbeite()
        {

            // Setzen des mutex
            mutex.WaitOne();
            
            
            //if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine("Another app instance is running. Bye!");
            //    Console.ForegroundColor = ConsoleColor.White;
            //    return;
            //}

            for (int i = 0; i < 100000; i++)
            {
                anzahl++;
            }

            // freigeben des mutex
            mutex.ReleaseMutex();
        }


    }
}
