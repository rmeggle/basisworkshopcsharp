﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppThreadMonitor
{
    class Program
    {
        static object sperre = new object();

        public static int anzahl = 0;

        public static void arbeite()
        {
            // ... umschreiben in Monitor.Enter und Monitor.Exit
            lock (sperre)
            {
                for (int i = 0; i < 100000; i++)
                {
                    anzahl++;
                }

            }

        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(arbeite);
            Thread t2 = new Thread(arbeite);
            Thread t3 = new Thread(arbeite);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.WriteLine("Gesamtanzahl : " + anzahl);

            Console.ReadLine();
        }
    }
}
