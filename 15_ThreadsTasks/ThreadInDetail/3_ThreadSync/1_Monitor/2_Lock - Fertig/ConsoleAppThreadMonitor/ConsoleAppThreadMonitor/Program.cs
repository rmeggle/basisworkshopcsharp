﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppThreadMonitor
{
    class Program
    {
        static object sperre = new object();

        public static int anzahl = 0;
        int i = 0;
        public static void arbeite()
        {
            Monitor.Enter(sperre);
            try
            {
                for (int i = 0; i < 100000; i++)
                {
                    anzahl++;
                    // bei einem Punkt soll zur Veranschaulichung gezielt
                    // eine exception ausgelöst werden:
                    if (anzahl == 200001) anzahl = anzahl / i;
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Fehler aufgetreten: " + ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
            finally
            {
                Monitor.Exit(sperre);
            }

        }

        static void Main(string[] args)
        {
            Thread t1 = new Thread(arbeite);
            Thread t2 = new Thread(arbeite);
            Thread t3 = new Thread(arbeite);
            t1.Start();
            t2.Start();
            t3.Start();
            Console.WriteLine("Gesamtanzahl : " + anzahl);

            Console.ReadLine();
        }
    }
}
