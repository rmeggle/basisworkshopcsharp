﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleAppEventWait
{
    class Program
    {
        // AutoResetEvent(true) würde gelich einen gesetzten Set() entsprechen
        public static AutoResetEvent autoresetevent = new AutoResetEvent(false);

        static void Robin1()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...gestartet");

            Thread.Sleep(1000);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...der andere Thread kann weitermachen");

            autoresetevent.Set(); // Setze das AutoResetEvent

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...warte nun selbst auf das AutoResetEvent");

            
            autoresetevent.WaitOne(); // Ab hier wartet er auf das Set()

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Thread.CurrentThread.Name + "...fertig");
            Console.ForegroundColor = ConsoleColor.White;
        }

        static void Robin2()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...gestartet");
            Console.WriteLine(Thread.CurrentThread.Name + "...warte nun auf auf das AutoResetEvent");

            autoresetevent.WaitOne(); // Ab hier wartet er auf das Set()
            

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...AutoResetEvent ist gesetzt, ich kann weitermachen!");

            autoresetevent.Set(); // Setze das AutoResetEvent

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Thread.CurrentThread.Name + "...fertig");
            Console.ForegroundColor = ConsoleColor.White;
        }

        static void Main(string[] args)
        {
            Thread r1 = new Thread(Robin1);
            Thread r2 = new Thread(Robin2);
            r1.Name = "Robin 1";
            r2.Name = "Robin 2";
            r1.Start();
            r2.Start(); 

            Console.WriteLine("Fertig");
            Console.ReadLine();
        }
    }
}
