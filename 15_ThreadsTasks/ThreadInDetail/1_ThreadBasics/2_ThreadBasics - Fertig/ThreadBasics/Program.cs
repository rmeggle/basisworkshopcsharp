﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading; // Namespace für Threading hingeführt

namespace ThreadBasics
{
    class Program
    {
        static void Main(string[] args)
        {

            // Standard
            Thread t1 = new Thread(new ThreadStart(M1));
            t1.Start();
            // Dem thread einen Namen geben: 
            t1.Name = "Ich bin thread t1";
            t1.Priority = ThreadPriority.Highest; // Thread eine priorität mitgeben

            // Verkürzte Schreibweise
            Thread t2 = new Thread(M1);
            t2.Start();

            // einfach nur starten
            new Thread(M1).Start();

            // Start mit delegate-Methode
            Thread t3 = new Thread(delegate () { M1(); } );
            t3.Start();

            // Start mit lambda-Syntax
            Thread t4 = new Thread(() => M1() );
            t4.Start();

            Thread t5 = new Thread( () => { Console.WriteLine("Bin anderer M1"); } );
            t5.Start();

            M1(); // Startet aus UI Thread

            Console.ReadLine();
        }

        static void M1()
        {
            Console.WriteLine("arbeite...");
        }
    }
}
