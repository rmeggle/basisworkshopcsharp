﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppBindingHandler_1
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        ClassPerson p = new ClassPerson();
        public MainWindow()
        {
            InitializeComponent();

            // irgendwoher (Eine Klasse ... eine Abfrage...)
            //textBoxVorname.Text = "Donald";
            //textBoxNachname.Text = "Duck";
            p.Vorname = "init";
            p.Nachname = "init";
            DataContext = p;
            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            String vn = p.Vorname;
            String nn = p.Nachname;
            p.Save();
        }
    }
}
