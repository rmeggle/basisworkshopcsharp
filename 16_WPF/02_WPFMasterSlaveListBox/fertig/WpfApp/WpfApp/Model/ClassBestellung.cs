﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Model
{
    class ClassBestellung : INotifyPropertyChanged
    {
        private string bestellnummer;
        public string Bestellnummer
        {
            get { return bestellnummer; }
            set
            {
                bestellnummer = value;
                OnPropertyChanged("Bestellnummer");
            }
        }

        private string artikel;
        public string Artikel
        {
            get { return artikel; }
            set
            {
                artikel = value;
                OnPropertyChanged("Artikel");
            }
        }

        private DateTime bestelldatum;
        public DateTime Bestelldatum
        {
            get { return bestelldatum; }
            set
            {
                bestelldatum = value;
                OnPropertyChanged("Bestelldatum");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }
}
