﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Model
{
    class ClassKunde : INotifyPropertyChanged
    {
        /// <summary>
        /// Vertragserfüllung aus dem Interface INotifyPropertyChanged:
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Jeder Kunde hat "n" Bestellungen. 
        /// </summary>
        private ObservableCollection<ClassBestellung> bestellung =  new ObservableCollection<ClassBestellung>();
        public ObservableCollection<ClassBestellung> Bestellung
        {
            get { return bestellung; }
        }


        private string vorname;
        /// <summary>
        /// Property: Vorname des Kunden
        /// </summary>
        public string Vorname
        {
            get { return vorname; }
            set
            {
                vorname = value;
                OnPropertyChanged("Vorname");
            }
        }

        private string nachname;
        /// <summary>
        /// Property: Nachname des Kunden
        /// </summary>
        public string Nachname
        {
            get { return nachname; }
            set
            {
                nachname = value;
                OnPropertyChanged("Nachname");
            }
        }

        /// <summary>
        /// Eventhandler: Wenn sich eine Eigenschaft ändert
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
