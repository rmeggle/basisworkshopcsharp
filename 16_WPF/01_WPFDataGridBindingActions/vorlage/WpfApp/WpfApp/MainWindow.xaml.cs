﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp.Model;
namespace WpfApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<ClassPerson> Personenliste = new List<ClassPerson>();

        public MainWindow()
        {
            InitializeComponent();

            Personenliste.Add(new ClassPerson() { Vorname = "Donald", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Dasy", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Dagobert", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Daniel", Nachname = "Düsentrieb" });

            DataContext = Personenliste;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Personenliste.Add(new ClassPerson() { Vorname = txtVorname.Text, Nachname = txtNachname.Text });

            String Info = String.Format("{0} {1} wurde der Liste hinzugefügt und hat nun {2} Elemente", txtVorname.Text, txtNachname.Text, Convert.ToString(Personenliste.Count()));

            MessageBox.Show(Info);
        }
    }
}
