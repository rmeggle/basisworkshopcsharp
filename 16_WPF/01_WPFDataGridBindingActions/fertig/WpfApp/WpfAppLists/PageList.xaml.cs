﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfAppLists;

namespace WpfAppLists
{
    /// <summary>
    /// Interaktionslogik für PageList.xaml
    /// </summary>
    public partial class PageList : Page
    {
        public List<ClassPerson> Personenliste = new List<ClassPerson>();

        public PageList()
        {
            InitializeComponent();
            
            Personenliste.Add(new ClassPerson() { Vorname = "Donald", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Dasy", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Dagobert", Nachname = "Duck" });
            Personenliste.Add(new ClassPerson() { Vorname = "Daniel", Nachname = "Düsentrieb" });

            DataContext = Personenliste;

        }
    }
}
