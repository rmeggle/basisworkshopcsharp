﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace WpfAppLists
{
    public class ClassPersonINotifyPropertyChanged : INotifyPropertyChanged
    {

        //public String Vorname { get; set; }
        //public String Nachname { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private String vorname;
        public String Vorname
        {
            get { return vorname; }
            set
            {
                vorname = value;
                OnPropertyChanged("Vorname");
            }
        }

        private String nachname;
        public String Nachname
        {
            get { return nachname; }
            set
            {
                nachname = value;
                OnPropertyChanged("Nachname");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
                Debug.WriteLine("Handler wurde ausgelöst: " + propertyName);
            }
            
        }
    }
}
