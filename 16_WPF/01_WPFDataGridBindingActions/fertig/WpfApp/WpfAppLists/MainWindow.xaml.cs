﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppLists
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonList_Click(object sender, RoutedEventArgs e)
        {
            FrameMain.Content = new PageList();
        }

        private void ButtonSimpleBinding_Click(object sender, RoutedEventArgs e)
        {
            FrameMain.Content = new PageSimpleBinding();
        }

        private void ObservableCollection_Click(object sender, RoutedEventArgs e)
        {
            FrameMain.Content = new PageObservableCollection();
        }

        private void INotifyPropertyChanged_Click(object sender, RoutedEventArgs e)
        {
            FrameMain.Content = new PageINotifyPropertyChanged();
        }
    }
}
