﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppLists
{
    /// <summary>
    /// Interaktionslogik für PageINotifyPropertyChanged.xaml
    /// </summary>
    public partial class PageINotifyPropertyChanged : Page
    {
        public ObservableCollection<ClassPersonINotifyPropertyChanged> Personenliste = new ObservableCollection<ClassPersonINotifyPropertyChanged>();

        public PageINotifyPropertyChanged()
        {
            InitializeComponent();
            Personenliste.Add(new ClassPersonINotifyPropertyChanged() { Vorname = "Donald", Nachname = "Duck" });
            Personenliste.Add(new ClassPersonINotifyPropertyChanged() { Vorname = "Dasy", Nachname = "Duck" });
            Personenliste.Add(new ClassPersonINotifyPropertyChanged() { Vorname = "Dagobert", Nachname = "Duck" });

            DataContext = Personenliste;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // Von Daniel Düsentrieb bekommt allerdings das DataGrid nichts mehr mit... 
            Personenliste.Add(new ClassPersonINotifyPropertyChanged() { Vorname = txtVorname.Text, Nachname = txtNachname.Text });

            String Info = String.Format("{0} {1} wurde der Liste hinzugefügt und hat nun {2} Elemente", txtVorname.Text, txtNachname.Text, Convert.ToString(Personenliste.Count()));

            MessageBox.Show(Info);

        }
    }
}
