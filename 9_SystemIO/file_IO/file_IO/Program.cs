﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace file_IO
{
    class Program
    {
        static String Textfile = @"\t8.shakespeare.txt";

        static void Main(string[] args)
        {
            ReadAllText();
            ReadAllLines();
            ReadFromWeb();

            //  .--------------.     .--------------.
            //  | SteamWriter  |     | SteamReader  |
            //  | BinaryWriter |     | BinaryReader |
            //  '--------------'     '--------------'
            //          |                    |
            //          |                    |
            //          |                    |
            //          |   .------------.   |
            //          '-->| FileStream |<--'
            //              '------------'
            //                     |
            //                     |
            //                     v
            //                 .-------.
            //                 | Datei |
            //                 '-------'

            // Datenströme (FileStreams) beschreiben jede Art von lesen/schreiben (Datei, Netzwerk, Tastaur...)

            // Den aktuellen Ordner ermitteln: 
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            // Prüfen, ob eine Datei existiert: 
            Boolean Existiert = System.IO.File.Exists(AktuellerOrdner + Textfile);
            // Prüfen, ob ein Verzeichnis existiert:
            Existiert = System.IO.Directory.Exists(AktuellerOrdner);
            // Eine Datei kopieren
            System.IO.File.Copy(AktuellerOrdner + Textfile, AktuellerOrdner + @"\copy.txt");
            // Eine Datei verschieben oder umbenenen: 
            System.IO.File.Move(AktuellerOrdner + @"\copy.txt", AktuellerOrdner + @"\copy1.txt");
            // Eine Datei löschen: 
            System.IO.File.Delete(AktuellerOrdner + @"\copy1.txt");
            // Alle Dateien in einem Ordner ermitteln:
            string[] AlleDateien = Directory.GetFiles(AktuellerOrdner);
            string[] AlleVerzeichnisse = Directory.GetDirectories(@"C:\");

            // Spezielle Ordner sind die "Bibliotheken" ab Windows 7
            string name = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);

            
        }

        static void ReadAllText()
        {
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            string text = System.IO.File.ReadAllText(AktuellerOrdner + Textfile);

            //System.Console.WriteLine("Contents of {0} = {1}", Textfile, text);
        }

        static void ReadAllLines()
        {
            string[] lines = System.IO.File.ReadAllLines(Textfile);
            int i=0;
            foreach (string line in lines)
            {
                Console.WriteLine(line);
                // Abbrechen nach 10 Zeilen... 
                i++;
                if (i > 10) break;
            }
        }

        static void ReadFromWeb()
        {
            String WebSource = @"https://ocw.mit.edu/ans7870/6/6.006/s08/lecturenotes/files/t8.shakespeare.txt";
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(WebSource);
            StreamReader reader = new StreamReader(stream);
            String content = reader.ReadToEnd();
        }

        static void FileStreamClass()
        {
            // Referenz: https://msdn.microsoft.com/en-us/library/system.io.filestream_methods(v=vs.110).aspx

        }

        static void FileStreamWriter()
        {
            String AktuellerOrdner = System.IO.Directory.GetCurrentDirectory();
            StreamWriter file2 = new StreamWriter(AktuellerOrdner + @"\file.txt", true); // das "true" steht dafür, dass Text angehängt (->append) werden soll
            file2.WriteLine("hallo Welt");
            file2.Close();
        }

    }
}
