﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Array-Größe bei der Initialisierung festlegen"

            int[] myArr;

            myArr = new int[3]; // drei(!) Element mit Index 0, 1, 2 vorbereitet - alle mit 0 vorinitialisiert
            myArr = new int[3] { 24, 29, 30 }; // drei Elemente mit Indiex 0, 1, 2 mit Werten vorinitialisiert
            //myArr = new int[3] { 24 }; // ... das geht nicht - wenn, dann müssen alle Werte für jedes Element vogegeben werden

            // mehrdimensionale Felder: 
            int [,] myArr2x2;
            myArr2x2 = new int[2, 2]; // matix mit 2x2 Feldern vorbereitet - alle mit 0 vorinitialisiert
            myArr2x2 = new int[2, 2] { { 0, 0 }, 
                                       { 0, 0 } };

            #endregion

            #region "Array-Größe zur Laufzeit festlegen"

            int[] myArrDyn;
            myArrDyn = new int[1];
            myArrDyn = new int[2];
            myArrDyn = new int[3];

            // Wenn die bisherigen Werte erhalten beiben sollen: Array.Resize<T>
            myArrDyn = new int[2];
            myArrDyn[0] = 1;
            myArrDyn[1] = 2;

            Array.Resize(ref myArrDyn, 3);
            myArrDyn[2] = 3;



            #endregion

            #region "Generische Datentypen: C# / .net Objekte als Datenfelder verwenden"

            // Definition: https://msdn.microsoft.com/en-us/library/system.collections.arraylist(v=vs.110).aspx
            ArrayList arraylist = new System.Collections.ArrayList();
            arraylist.Add(99);
            arraylist.Add(1);
            arraylist.Add(2);
            arraylist.Add(2);
            arraylist.Add(3);
            arraylist.Add(4);
            arraylist.Add("foobar"); // Kein Fehler beim Übersetzen des Quellcodes

            //foreach (int Element in array1) //-->Laufzeitfehler, da Element nr. 2 ist ein String
            //{
            //    Console.WriteLine(Element); 
            //}

            foreach (object Element in arraylist)
            {
                Console.WriteLine(Element); // Ausgabe: 99,1,2,2,3,4,foobar (mit doublette)

            }

            // Definition: https://msdn.microsoft.com/en-us/library/6sh2ey19(v=vs.110).aspx
            List<int> list = new List<int>();
            list.Add(99);
            list.Add(1);
            list.Add(2);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            //list1.Add("foobar"); //<-- Fehler beim Übersetzen des Quellcodes, das List if Integer
            foreach (int Element in list)
            {
                Console.WriteLine(Element); // Ausgabe: 99,1,2,2,3,4 (mit doublette)
            }

            // Definition: https://msdn.microsoft.com/en-us/library/bb359438(v=vs.110).aspx
            var hashset = new HashSet<int>();

            hashset.Add(99);
            hashset.Add(1);
            hashset.Add(2);
            hashset.Add(2);
            hashset.Add(3);
            hashset.Add(4);
            hashset.Add(5);
            foreach (int Element in hashset)
            {
                Console.WriteLine(Element); // Ausgabe: 99,1,2,3,4 (ohne doublette!)
            }


            #endregion

            Console.ReadLine();

        }
    }
}
