﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP4_Polimorphismus_new
{

    #region "Klassen"
    class Person
    {
        /// <summary>
        /// Methode von Person
        /// </summary>
        public virtual void Personendaten()
        {
            Console.WriteLine("Personendaten aus Klasse Person");
        }

    }

    class Mitarbeiter : Person
    {
        /// <summary>
        /// Methode von Mitarbeiter überschreibt die Methode Personendaten aus Person
        /// </summary>
        public new void Personendaten()
        {
            Console.WriteLine("Personendaten aus Klasse Mitarbeiter");
        }
    }

    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            string trenner = new string('-', 45);

            Person p1 = new Person();
            Mitarbeiter m1 = new Mitarbeiter();

            p1.Personendaten(); // "Personendaten aus Klasse Person"
            m1.Personendaten(); // "Personendaten aus Klasse Mitarbeiter"

            Console.WriteLine(trenner);

            Person p2 = new Mitarbeiter();
            p2.Personendaten(); // "Personendaten aus Klasse Person" (da new und nicht overwrite bei Instanzbildung)

            Mitarbeiter m2 = new Mitarbeiter();
            Person p3 = (Person)m2; // Typecast von Mitarbeiter m2 auf Referenztyp Person 
            p3.Personendaten(); // "Personendaten aus Klasse Person" (da new und nicht overwrite bei Instanzbildung)

            // Wenn eine (abgeleitete) Klasse einen virtuellen "Member" überschreibt, wird dieser 
            // "Member" bei >>new<< der Basisklasse aufgerufen. Dies stellt grundlegend den Unterschied
            // zwischen >>overwrite<< und >>new<< bei der >>polimorphie<< dar. 

            Console.ReadLine();
        }
    }
}
