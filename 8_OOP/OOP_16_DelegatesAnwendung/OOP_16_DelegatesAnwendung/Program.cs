﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_16_DelegatesAnwendung
{
    class Program
    {
        static string trenner = new string('-', 45);

        static void Main(string[] args)
        {
            

            List<ClassPerson> Personen = new List<ClassPerson>();
            #region "Personenliste füllen"
            Personen.Add(new ClassPerson() { Vorname = "Donald", Nachname = "Duck", Geburtstag = new DateTime(1950, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Dasy", Nachname = "Duck", Geburtstag = new DateTime(1956, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Dagobert", Nachname = "Duck", Geburtstag = new DateTime(1950, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Daniel", Nachname = "Düsentrieb", Geburtstag = new DateTime(1950, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Tick", Nachname = "Duck", Geburtstag = new DateTime(2010, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Trick", Nachname = "Duck", Geburtstag = new DateTime(2010, 1, 1) });
            Personen.Add(new ClassPerson() { Vorname = "Track", Nachname = "Duck", Geburtstag = new DateTime(2010, 1, 1) });
            #endregion

            #region "Umsetzung ohne Delegates"

            //Console.WriteLine("Alle volljährige Personen: "); Console.WriteLine(trenner); 
            //foreach (ClassPerson p in Personen)
            //{
            //    if (p.Geburtstag.AddYears(18)<DateTime.Now)
            //    {
            //        Console.WriteLine("{0} {1}", p.Vorname, p.Nachname);
            //    }
            //}
            //Console.WriteLine("\nAlle minderjährige Personen: "); Console.WriteLine(trenner);
            //foreach (ClassPerson p in Personen)
            //{
            //    if (p.Geburtstag.AddYears(18) > DateTime.Now)
            //    {
            //        Console.WriteLine("{0} {1}", p.Vorname, p.Nachname);
            //    }
            //}
            //
            //ClassPerson.AlleMinderjährige(Personen);
            //ClassPerson.AlleVolljährige(Personen);

            #endregion

            #region "Umsetzung MIT Delegates"

            listePersonAuf erwachsene = new listePersonAuf(erwachsenePersonen);
            Console.WriteLine("Alle volljährige Personen: "); Console.WriteLine(trenner);
            ClassPerson.Personenliste(Personen, erwachsene);

            listePersonAuf minderjährige = new listePersonAuf(minderjährigePersonen);
            Console.WriteLine("\nAlle minderjährige Personen: "); Console.WriteLine(trenner);
            ClassPerson.Personenliste(Personen, minderjährige);

            #endregion

            Console.ReadLine();

        }

        public static bool erwachsenePersonen(ClassPerson Person)
        {
            return (Person.Geburtstag.AddYears(18) < DateTime.Now) ? true : false;
        }

        public static bool minderjährigePersonen(ClassPerson Person)
        {
            return (Person.Geburtstag.AddYears(18) > DateTime.Now) ? true : false;
        }

    }
}
