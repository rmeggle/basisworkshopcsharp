﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_12_Kopierkonstruktoren
{
    #region "Klassen"
    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }

        /// <summary>
        /// Standardkonstruktor von Person
        /// </summary>
        public Person()
        {
        }

        public Person(Person anderesPersonenObjekt)
        {
            this.Vorname = anderesPersonenObjekt.Vorname;
            this.Nachname = anderesPersonenObjekt.Nachname;

        }

        /// <summary>
        /// Methode von Person
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }
    }

    #endregion

    class Program
    {
        static void Main(string[] args)
        {

            Person p1 = new Person() { Vorname = "Donald", Nachname = "Duck" } ;
            p1.Personendaten();
            Person p2 = new Person(p1);
            p2.Personendaten();

            Console.ReadLine();
        }
    }
}
