﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_10_StatischerKonstruktor
{

    #region "Klassen"
    class Person
    {
        public string vorname { get; set; }
        public string nachname { get; set; }

        /// <summary>
        /// Statischer Konstruktor der Klasse "Person". Statische Konstruktoren dürfen über keine
        /// Zugriffsklassifizierungen wie public, private... verfügen. Weiterhin: 
        /// * er darf keine Parameter erhalten
        /// * kann nicht dirket aufgerufen werden
        /// * programmiertechnisch kann nicht gesteuert werden, wann er aufgerufen wird
        /// </summary>
        static Person()
        {
            // Durch "static" wird erzwungen, den überladenen Constructor zu verwenden!

            // Daher: Hier wäre ein guter Platz z.B. für eine automatische Protokollierung
            DateTime jetzt = DateTime.Now;
            Console.WriteLine("{0} - Konstruktor von Class Person wurde aufgerufen", jetzt);
        }

        /// <summary>
        /// Jedoch kann man weitere Konstruktoren setzten, welche über eine andere Signatur
        /// initiiert werden
        /// </summary>
        /// <param name="Vorname"></param>
        /// <param name="Nachname"></param>
        public Person(String Vorname, String Nachname)
        {
            this.vorname = Vorname;
            this.nachname = Nachname;
        }

        /// <summary>
        /// Methode von Person
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.vorname, this.nachname);
        }
    }


    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            //Person p1 = new Person(); // fehler
            Person p2 = new Person("Donald", "Duck");
            Console.ReadLine();
        }
    }
}
