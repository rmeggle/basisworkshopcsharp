﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_3_Vererbung
{

    #region "Klassen"
    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }

        /// <summary>
        /// Standardkonstruktor von Person
        /// </summary>
        public Person()
        {
        }

        /// <summary>
        /// Methode von Person
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine ("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }
    }

    class Mitarbeiter : Person
    {
        public String Abteilung { get; set; }

        public void Mitarbeiterdaten()
        {
            Console.WriteLine("Mitarbeiterdaten: {0}", this.Abteilung);
        }
    }
    #endregion


    class Program
    {
        static void Main(string[] args)
        {
            string trenner = new string('-', 45);

            Person p1 = new Person();
            Mitarbeiter m1 = new Mitarbeiter();

            p1.Vorname = "Donald";
            p1.Nachname = "Duck";

            p1.Personendaten();

            m1.Vorname = "Daniel";
            m1.Nachname = "Düsentrieb";
            m1.Abteilung = "Forschng";

            m1.Personendaten();
            m1.Mitarbeiterdaten();

            Console.WriteLine(trenner);

            // Direktes Setzten von Eigenschaften beim Konstruktor
            // als Alternative z.B. für das Überladen des Konstruktors

            Person p2 = new Person { Vorname = "Dagobert", Nachname = "Duck" };
            p2.Personendaten();

            // ...kann auch per Variablenzuweisung geschehen: 
            String V1 = "Dasy";
            String V2 = "Duck";

            Person p3 = new Person { Vorname = V1 , Nachname = V2 };
            p3.Personendaten();

            Console.WriteLine(trenner);

            Person p4 = new Mitarbeiter();
            p4.Personendaten(); // Kein Zugriff auf Mitarbeiter, da p2 vom Typ Person ist

            // Mitarbeiter m2 = new Person(); // das geht nun wieder nicht... 

            Mitarbeiter m3 = new Mitarbeiter { Vorname = "Trick", Nachname = "Track", Abteilung = "Schule" };
            m3.Personendaten();
            m3.Mitarbeiterdaten();

            Console.ReadLine();

        }
    }
}
