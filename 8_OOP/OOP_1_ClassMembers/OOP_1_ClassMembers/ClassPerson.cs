﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1_ClassMembers
{
    class ClassPerson
    {
        #region "Klassenvariablen / Felder"
        //
        // Generell: Klassenvariablen nennt man Klassenfelder oder kurz nur "Felder"
        //
        // Sichtbarkeit wird Durch Angabe eines Schlüsselwortes gekennzeichnet: 
        // internal : Auf interne Typen oder Member kann nur in Dateien derselben Assembly zugegriffen werden
        internal int foobar1 = 0;
        // private  : Private Member sind nur innerhalb des Klassen- oder Strukturtexts zugreifbar, in dem sie deklariert wurden
        private int foobar2 = 0;
        // proteced : Ein Member mit dem Zugriffsmodifizierer protected einer Basisklasse ist nur dann in einer abgeleiteten Klasse zugreifbar, wenn der Zugriff über den abgeleiteten Klassentyp erfolgt.
        protected int foobar3 = 0;
        // public   : Es gibt keine Beschränkungen auf zugreifende öffentliche Member, es kann immer darauf Zugegriffen werden
        public int foobar4 = 0;
        #endregion

        #region "Eigenschaften"
        /// <summary>
        /// öffentlich lesend und schreibend zugreifbar
        /// </summary>
        public int MyProperty1 { get; set; }
        // 
        /// <summary>
        /// öffentlich lesend zugreifbar - privat schreibbar 
        /// </summary>
        public int MyProperty2 { get; private set; }
        /// <summary>
        /// entspricht in ausgeschriebener Schreibweise: 
        /// </summary>
        private int myproperty2_1;
        public int MyProperty2_1
        {
            get
            {
                return myproperty2_1;
            }
            private set
            {
                myproperty2_1 = value;
            }
        }

        private int MyProperty3 { get; set; }
        public int MyProperty { get; set; }
        #endregion

        #region "Konstruktoren/Destruktoren"

        //
        // Konstruktoren werden aufgerufen, wenn ein Objekt durch "new" instanziert wird. 
        //

        // 
        // öffentliche Konstruktoren werden verwendet, 

        /// <summary>
        /// Ein Konstruktor, der keine Parameter akzeptiert, wird Standardkonstruktor genannt...
        /// </summary>
        public ClassPerson()
        {
        }


        #endregion

        #region "Methoden"

        /// <summary>
        /// Beispiel einer Methode ohne Rückgabe => eine Prozedur
        /// </summary>
        public void meineProzedur()
        {

        }

        /// <summary>
        /// Beispiel einer Methode mit Rückgabe => Eine Funktion
        /// </summary>
        /// <returns></returns>
        public int meineFunktion()
        {
            return 0;
        }

        #endregion

    }
}
