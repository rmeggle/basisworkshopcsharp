﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_13_Destruktoren
{
    #region "Klassen"
    class Person
    {
        public String Vorname { get; set; }
        public String Nachname { get; set; }

        /// <summary>
        /// Standardkonstruktor von Person
        /// </summary>
        public Person()
        {
            Console.WriteLine("Objekt Person ist erstellt");
        }

        /// <summary>
        /// Destruktor... auch "Finalizier" genannt, wird ohne Zugriffskontrolle (private, public... ) definiert
        /// Destruktoren werden automatisch aufgerufen - sie können nicht explizit aufgerufen werden!
        /// Sie werden aufgerufen, wenn kein Code mehr sie verwendet. Der Garbage Collector ist hierfür verwantwortlich
        /// </summary>
        ~Person()
        {
            System.Diagnostics.Trace.WriteLine("Objekt Person ist freigegeben"); // Schreibt eine Ausgabe in "output" vom Visual Studio
        }

        /// <summary>
        /// Methode von Person
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }
    }

    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            // erstellen von Person 1
            Person p1 = new Person() { Vorname = "Donald", Nachname = "Duck" };
        }
    }
}
