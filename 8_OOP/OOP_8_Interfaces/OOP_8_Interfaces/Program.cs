﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_8_Interfaces
{
    // Grundregel: Interfaces sind eine Art "Vertrag": Klassen, die ein Interface beantragen haben die Bringschuld, der im Vertrag genannten Member zu haben

    interface IMensch
    {
        // Jeder Mensch hat eine Augenfarbe
        String Augenfarbe { get; set}
    }

    interface IPersonen
    {
        // Jede Person muss einen Vor- und Nachnamen haben
        String Vorname { get; set; }
        String Nachname { get; set; }
        // Jede Person muss eine Methode Personendaten haben: 
        void Personendaten();
    }

    class Person : IPersonen, IMensch
    {
        public String Vorname { get; set; }     // Aus dem Interface: IPersonen
        public String Nachname { get; set; }    // Aus dem Interface: IPersonen
        public String Augenfarbe { get; set; }  // Aus dem Interface: IMenschen

        /// <summary>
        /// Methode Personendaten, wie im Interface IPersonen gefordert
        /// </summary>
        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }

        /// <summary>
        /// Neue Implementierung von Adressdaten in Mitarbeiter, davon steht nichts im Interface,
        /// kann aber in der Klasse durchaus erweitert werden (=> Interface beschreibt das "minimum")
        /// </summary>
        public void Adressdaten()
        {
            Console.WriteLine("Adressdaten von Mitarbeiter");
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person() { Vorname = "Donald", Nachname = "Duck", Augenfarbe="schwarz" };

            p1.Personendaten();
            p1.Adressdaten();
            Console.ReadLine();

        }
    }
}
