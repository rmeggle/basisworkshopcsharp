﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Ein Delegate ist ein typsicherer Funktionszeiger. Er zeigt (referenziert) auf eine statische Funktion innerhalb einer Klasse.
/// Die Signatur des Delegates muss mit der Signatur der Funktion übereinstimmen auf die der Delegate zeigt.
/// Generell ist ein Delegate identisch mit einer Klasse. Es kann eine Instanz erzeugt werden und der Funktionsname zeigt auf den 
/// delegate Konstruktur -> und er zeigt auf nur diese (statische) Funktion der Klasse. 
/// </summary>
namespace OOP_15_Delegates_simple
{
    /// <summary>
    /// Definieren des Delegates "SagHalloWelt". Er soll auf die Funktion "HalloVonClassPerson" der Klasse ClassPerson zeigen. 
    /// Die Funktion "HalloVonClassPerson" hat in der Signatur einen Übergabeparameter vom Typ String. 
    /// </summary>
    /// <param name="Nachricht"></param>
    public delegate void SagHalloWelt(string Nachricht);

    class Program
    {
        static void Main(string[] args)
        {
            // Zugriff auf ClassPerson durch Instanz
            ClassPerson p1 = new ClassPerson();
            p1.Vorname = "Donald";
            p1.Nachname = "Duck";

            // Bilden einer Instanz des Delegates und verweisen auf die Funktion "HalloVonClassPerson"
            // der Klasse ClassPerson
            SagHalloWelt MeinHalloWeltDelegate = new SagHalloWelt(ClassPerson.HalloVonClassPerson);

            // Aufrufen der (statischen) Funktion "HalloVonClassPerson" 
            MeinHalloWeltDelegate("Hallo Welt");

            Console.ReadLine();
        }


    }
}
