﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_15_Delegates_simple
{
    class ClassPerson
    {

        public String Vorname { get; set; }
        public String Nachname { get; set; }

        public ClassPerson() { }

        public void Personendaten()
        {
            Console.WriteLine("Personendaten: {0} {1}", this.Vorname, this.Nachname);
        }

        public static void HalloVonClassPerson(String DieNachricht)
        {
            Console.WriteLine(DieNachricht);
        }

    }
}
