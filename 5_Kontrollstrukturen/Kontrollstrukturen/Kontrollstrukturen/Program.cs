﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kontrollstrukturen
{
    class Program
    {
        static void Main(string[] args)
        {
            ifstatements();
            danglingelse();
            switchStatement();

            Console.ReadKey();
        }

        static void ifstatements()
        {
            // Referenz: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/if-else

            bool Condition1 = true;
            bool Condition2 = true;
            bool Condition3 = true;
            bool Condition4 = true;

            if (Condition1)
            {
                // Condition1 is true.
            }
            else if (Condition2)
            {
                // Condition1 is false and Condition2 is true.
            }
            else if (Condition3)
            {
                if (Condition4)
                {
                    // Condition1 and Condition2 are false. Condition3 and Condition4 are true.
                }
                else
                {
                    // Condition1, Condition2, and Condition4 are false. Condition3 is true.
                }
            }
            else
            {
                // Condition1, Condition2, and Condition3 are false.
            }

            // BEISPIEL: 
            Console.Write("Gib mir einen Buchstaben: ");
            char ch = (char)Console.Read();

            if (Char.IsUpper(ch))
            {
                Console.WriteLine("Es ist ein Großbuchstabe!");
            }
            else if (Char.IsLower(ch))
            {
                Console.WriteLine("Es ist ein Kleinbuchstabe!");
            }
            else if (Char.IsDigit(ch))
            {
                Console.WriteLine("Es ist eine Zahl!");
            }
            else
            {
                Console.WriteLine("Das ist nicht Alphanumerisch!");
            }

        }

        static void danglingelse()
        {
            Console.Write("Gib eine Zeichenkett ein: ");
            String console = Console.ReadLine();
            if (console.Length>2)
                if (console.Length > 4) // "" oder ''?
                    Console.WriteLine("vier Buchstaben...");
                else
                    Console.WriteLine("dangling else!");
            // "dangling else" => Wohin gehört es? = Wie wird es deutlicher wohin es gehört?
        }

        static void switchStatement()
        {
            // Referenz: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/switch
            Console.Write("Gib ein a, ein b oder einen anderen Buchstaben ein: ");
            char ch = (char)Console.Read();
            switch (ch)
            {
                case 'a':
                    Console.WriteLine("a");
                    break;
                case 'b':
                    Console.WriteLine("b");
                    break;
                default:
                    Console.WriteLine("ein anderer Buchstabe...");
                    break;
            }

            // Anderes Beispiel:
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Saturday:
                    Console.WriteLine("Das Wochenende");
                    break;
                case DayOfWeek.Monday:
                    Console.WriteLine("Erster Arbeitstag einer Woche (Montag).");
                    break;
                case DayOfWeek.Friday:
                    Console.WriteLine("Letzter Arbeitstag einer Woche (Freitag).");
                    break;
                default:
                    Console.WriteLine("Ein anderer Tag der Woche");
                    break;
            }
        }
    }
}
