﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;

namespace xpath
{
    class Program
    {
        static void Main(string[] args)
        {
            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";
            string trenner = new string('-', 45);

            XPathDocument XMLdoc = new XPathDocument(XMLFile);
            XPathNavigator XMLnavi = XMLdoc.CreateNavigator();
            XPathNodeIterator XMLNodeIterator;

            // Cursor auf root stellen
            XMLnavi.MoveToRoot();

            // Verschieben des Cursors auf den ersten Knoten und Ausgabe: 
            XMLnavi.MoveToFirstChild();
            Console.WriteLine("Der erste Knoten: " + XMLnavi.Name);
            // Verschieben des Cursors auf den nächsten Knoten und Ausgabe: 
            XMLnavi.MoveToFirstChild();
            Console.WriteLine("Der nächste Knoten: " + XMLnavi.Name);
            // Verschieben des Cursors auf den nächsten Knoten und Ausgabe: 
            XMLnavi.MoveToFirstChild();
            Console.WriteLine("Der nächste Knoten: " + XMLnavi.Name);

            // Cursor wieder root stellen
            XMLnavi.MoveToRoot();

            // Verschieben des Cursors auf den ersten Knoten und Ausgabe: 
            XMLnavi.MoveToFirstChild();
            Console.WriteLine("Der erste Knoten: " + XMLnavi.Name);

            

            XMLnavi.MoveToChild("Print", "");
            XMLnavi.MoveToChild("File", "");
            // alle untergeordneten Elemente einer 'Person'
            XMLNodeIterator = XMLnavi.SelectDescendants("", "", false); // "Descendands" -> "untergeordneten"
            Console.WriteLine("Alle untergeordneten Elemente von 'File':");
            Console.WriteLine(trenner);
            while (XMLNodeIterator.MoveNext())
            {
                Console.WriteLine(XMLNodeIterator.Current.Name);
            }


            // Cursor wieder root stellen
            XMLnavi.MoveToRoot();
            double result = Convert.ToDouble(XMLnavi.Evaluate("count(//JPGs//Print/File/FileName)"));
            Console.WriteLine("Anazhl der Bilder in der XML-Datei = {0}", result);
            Console.ReadLine();

            // Ausführliche Dokumentation hier: https://msdn.microsoft.com/en-us/library/ms256471.aspx

        }
    }
}
