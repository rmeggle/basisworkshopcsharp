﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace xmlDocument
{

    class Program
    {
        static void Main(string[] args)
        {
            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";
            string trenner = new string('-', 45);

            // Im Gegensatz zu XMLreader und XPath wird bei XMLDocument das gesamte 
            // XML in den Speicher gelesen und als DOM ( Document Object Model) mittels XPath behandelt.
            // Beispiel: 
            XmlDocument XMLdoc = new XmlDocument();
            XMLdoc.Load(XMLFile);

            System.Xml.XPath.XPathNavigator XMLnavi = XMLdoc.CreateNavigator(); // Und weiter geht es mit XPAth...

            // oder alternativ:
            XmlNode root = XMLdoc.DocumentElement;
            Console.WriteLine("Wurzelelement:\n {0}\n", root.Name);
            Console.WriteLine("Das gesamte Dokument:\n {0}\n", root.OuterXml);
            Console.WriteLine("Das innere von root (JPGs):\n {0}\n", root.InnerXml);

            XmlNodeList liste = XMLdoc.SelectNodes("//JPGs//Print//File//FileName");
            foreach (XmlNode temp in liste)
                Console.WriteLine("Dateiname: {0}", temp.InnerXml);

            // Manipulieren geht aber auch: 

            Console.WriteLine(trenner);

            XmlElement el = (XmlElement)XMLdoc.SelectSingleNode("//JPGs//Print//File//FileName[text()='bd100100128158-0000.jpg']");
            el.ParentNode.RemoveChild(el);

            XmlNodeList neueliste = XMLdoc.SelectNodes("//JPGs//Print//File//FileName");
            foreach (XmlNode temp in neueliste)
                Console.WriteLine("Dateiname: {0}", temp.InnerXml);

            Console.ReadLine();
        }
    }
}
