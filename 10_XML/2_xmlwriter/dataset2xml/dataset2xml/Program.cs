﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dataset2xml
{
    class Program
    {
        static void Main(string[] args)
        {
            // Erstellen eines DataSet's: 

            DataSet ds = new DataSet("DS");
            ds.Namespace = "Bilddaten";
            DataTable stdTable = new DataTable("Print");
            DataColumn col1 = new DataColumn("FileName");
            DataColumn col2 = new DataColumn("BackPrint");
            DataColumn col3 = new DataColumn("FileCRC");
            stdTable.Columns.Add(col1);
            stdTable.Columns.Add(col2);
            stdTable.Columns.Add(col3);
            ds.Tables.Add(stdTable);

            //Neue Zeile in das Dataset anlegen: 
            DataRow newRow; newRow = stdTable.NewRow();
            newRow["FileName"] = "bd100100128158-0000.jpg";
            newRow["BackPrint"] = "Bild einer Welt";
            newRow["FileCRC"] = "4164605383";
            stdTable.Rows.Add(newRow);
            newRow = stdTable.NewRow();

            //Eine weitere Zeile in das Dataaset anlegen:
            newRow["FileName"] = "bd100100128158-0001.jpg";
            newRow["BackPrint"] = "Bild einer Welt";
            newRow["FileCRC"] = "416460589";
            stdTable.Rows.Add(newRow);
            newRow = stdTable.NewRow();

            // usw....

            ds.AcceptChanges();

            // Abpeichern des Datasets als XML
            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";
            ds.WriteXml(XMLFile);
            
        }
    }
}
