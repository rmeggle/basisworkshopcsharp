﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace xmlwriter
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlWriter xmlWriter = XmlWriter.Create(System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml");

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("JPGs");

            xmlWriter.WriteStartElement("Print");
            xmlWriter.WriteAttributeString("Artikel", "4711");
            xmlWriter.WriteAttributeString("Kopien", "1");
            xmlWriter.WriteAttributeString("Gruppe", "");
            xmlWriter.WriteAttributeString("Surface", "2");

            xmlWriter.WriteStartElement("File");
            xmlWriter.WriteStartElement("FileName");
            xmlWriter.WriteString("bd100100128158-0000.jpg");
            xmlWriter.WriteStartElement("BackPrint");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("FileCRC");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


            xmlWriter.Close();
        }
    }
}
