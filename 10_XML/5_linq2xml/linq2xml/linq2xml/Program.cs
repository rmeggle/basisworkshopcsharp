﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace linq2xml
{
    class Program
    {
        static void Main(string[] args)
        {
            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";
            string trenner = new string('-', 45);

            XElement root = XElement.Load(XMLFile);

            IEnumerable<XElement> marketing =
                from el in root.Elements("Print")
                where(string)el.Attribute("Surface") == "01"
                select el;

            Console.WriteLine("Alle mit Surface 01:");
            foreach (XElement el in marketing)
                Console.WriteLine(el);


            Console.WriteLine(trenner);
            

            IEnumerable<XElement> EinSpezifischesBild =
                from el in root.Elements("Print").Elements("File")
                where (string)el.Element("FileName") == "bd100100128158-0001.jpg"
                select el;

            foreach (XElement el in EinSpezifischesBild)
                Console.WriteLine(el);

            Console.WriteLine(trenner);

            var sortedElements =
                from el in root.Elements("Print").Elements("File")
                orderby (string)el.Element("FileName") descending
                select new
                {
                    FileName = (string)el.Element("FileName"),
                    BackPrint = (string)el.Element("BackPrint"),
                    FileCRC = (string)el.Element("FileCRC")
                };

            foreach (var el in sortedElements)
                Console.WriteLine("FileName: {0} BackPrint: {1} FileCRC: {2:C0}",
                    el.FileName, el.BackPrint, el.FileCRC);


            Console.ReadLine();
        }
    }
}
