﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xml2dataset
{
    class Program
    {
        static void Main(string[] args)
        {
            // Dataset erstellen
            DataSet ds = new DataSet("DS");
            ds.Namespace = "Bilddaten";
            DataTable stdTable = new DataTable("Print");
            DataColumn col1 = new DataColumn("FileName");
            DataColumn col2 = new DataColumn("BackPrint");
            DataColumn col3 = new DataColumn("FileCRC");
            stdTable.Columns.Add(col1);
            stdTable.Columns.Add(col2);
            stdTable.Columns.Add(col3);
            ds.Tables.Add(stdTable);

            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";

            ds.ReadXml(XMLFile);

            Console.WriteLine(ds.GetXml() );
            Console.ReadLine();
        }
    }
}
