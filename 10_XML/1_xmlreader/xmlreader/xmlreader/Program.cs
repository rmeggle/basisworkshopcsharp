﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace xmlreader
{
    class Program
    {
        static void Main(string[] args)
        {
            String XMLFile = System.IO.Directory.GetCurrentDirectory() + @"/../../XMLFile.xml";
            XmlReader reader = XmlReader.Create(XMLFile);

            do
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        Console.Write("<{0}", reader.Name);
                        while (reader.MoveToNextAttribute())
                        {
                            Console.Write(" {0}='{1}'", reader.Name, reader.Value);
                        }
                        Console.WriteLine(">");
                        break;
                    case XmlNodeType.Text:
                        Console.Write(reader.Value);
                        break;
                    case XmlNodeType.EndElement:
                        Console.Write("</{0}>", reader.Name);
                        break;
                }
            } while (reader.Read());

            Console.ReadLine();
        }
    }
}
