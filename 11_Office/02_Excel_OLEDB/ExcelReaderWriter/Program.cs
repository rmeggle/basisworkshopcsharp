﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace ExcelReaderWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            String connString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\tmp\ExcelReaderWriter\Mappe1.xlsx;Extended Properties=""Excel 12.0 Xml; HDR = YES"";";
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                try
                {
                    conn.Open();
                    for (int i = 0; i<=10000; i++)
                    { 
                        String SQL = String.Format("Insert into [Tabelle1$] (id,name) values('Donald{0}','{1}')",i,i);

                        using (OleDbCommand cmd = new OleDbCommand(SQL, conn))
                        {
                            cmd.ExecuteNonQuery();
                        }
                    }

                    conn.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Das war nix: " + ex.Message);
                    //throw Exception(ex.Message, ex); ;
                }
            }
            Console.WriteLine("Fertig");
            Console.ReadLine();
        }
    }
}
